/* * * * Includes * * * */
/* * Core * */
#include "stm32f4xx.h"

/* * User libs * */
#include "libs/defines.h"
#include "libs/adc/adc.h"
#include "libs/timers/timer.h"
#include "libs/usart/usart.h"

/* * C libs * */
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
char mensaje_var[6] = "var = ";

int main(void) {

	/* * * * Hardware initialization * * * */
	/* * PC1 as ADC * */
	ADC_INIT();

	TIM3_CONFIG();

	INIT_USART2();

	while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); // Espera que el registro de transmision este libre
		USART_SendData(USART2, mensaje_var[1]); // Envia un caracter
	while (1) {
//		if (fTimer == 1) {
//			fTimer = 0;
//			ADC_PROCESSING();
//		} else if (fAdc == 1) {
//			fAdc = 0;
//			USART_Cmd(USART2, ENABLE);
//			SEND_DATA(MAX_SAMPLES, aAdc);
//			USART_Cmd(USART2, DISABLE);
//		}
			}
}

/* * * * Interruption handlers * * * */
void TIM3_IRQHandler(void) {
	if (TIM_GetITStatus(TIM3, TIM_IT_CC1) != RESET) {

		fTimer = 1;

		TIM_ClearITPendingBit(TIM3, TIM_IT_CC1);
	}
}

/* * * * Local functions * * * */
void ADC_PROCESSING(void) {
	if (i < MAX_SAMPLES) {
		aAdc[i] = ADC_READ();
		i++;
	} else {
		i = 0;
		fAdc = 1;
		TIM_Cmd(TIM3, DISABLE);
	}
}

