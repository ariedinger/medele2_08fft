#ifndef LIBS_USART_USART_H_
#define LIBS_USART_USART_H_

#include "stm32f4xx.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_rcc.h"

USART_InitTypeDef USART_InitStructure;

void INIT_USART2(void);
void SEND_DATA(long N_MAX, float* message);

#endif /* LIBS_USART_USART_H_ */
