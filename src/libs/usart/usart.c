#include "libs/usart/usart.h"

void INIT_USART2(void)
{
		USART_InitTypeDef USART_InitStructure;
		GPIO_InitTypeDef GPIO_InitStructure;

		/* --------------------------- System Clocks Configuration -----------------*/
		/* USART2 clock enable */
		RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE);

		/* GPIOA clock enable */
		RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);

		/*-------------------------- GPIO Configuration ----------------------------*/
		GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2 | GPIO_Pin_3;
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
		GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
		GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
		GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
		GPIO_Init(GPIOA, &GPIO_InitStructure);

		/* Connect USART pins to AF */
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2);
		GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2);


		/* USARTx configuration ------------------------------------------------------*/
		/* USARTx configurada como sigue:
		- BaudRate = 9600 baud
		- Largo de palabra = 8 Bits
		- Un Bit de stop
		- Paridad par
		- Control de flujo por hardware deshabilitado (RTS and CTS signals)
		- Recepcion y transmision habilitadas
		*/
		USART_InitStructure.USART_BaudRate = 9600;
		USART_InitStructure.USART_WordLength = USART_WordLength_9b;
		USART_InitStructure.USART_StopBits = USART_StopBits_1;
		USART_InitStructure.USART_Parity = USART_Parity_Even;
		USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;

		USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;

		USART_Init(USART2, &USART_InitStructure);

		USART_Cmd(USART2, ENABLE);
}

void SEND_DATA(long N_MAX, float* message)
{
	char buffer[N_MAX];

	for (uint8_t i=0; i<N_MAX; i++) // Envia "var = "
	{
		sprintf(buffer[i],"%f", message[i]);

		while(USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET); // Espera que el registro de transmision este libre
			USART_SendData(USART2, buffer[i]); // Envia un caracter
	}
}
