/*
 * timer.h
 *
 *  Created on: Jun 6, 2023
 *      Author: ariedinger
 */

#ifndef LIBS_TIMERS_TIMER_H_
#define LIBS_TIMERS_TIMER_H_

#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_tim.h"

NVIC_InitTypeDef NVIC_InitStructure;
TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

void TIM3_CONFIG(void);

#endif /* LIBS_TIMERS_TIMER_H_ */
