#include "libs/timers/timer.h"

void TIM3_CONFIG(void) {
	/* TIM3 clock enable */
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);

	/* Enable the TIM3 gloabal Interrupt */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* -----------------------------------------------------------------------
	 Configuracion TIM3 : Output Compare Timing Mode:

	 IMPORTANTE: el TIM3 toma su base de tiempo del APB1 Timer clock
	 -----------------------------------------------------------------------

	 Para 32 puntos en memoria:
	 1                                         1
	 f = ------------------------------------------------ = ------------------------------
	 [1/(SystemCoreClock /2)]*(TIM_PERIOD+1)*muestras	 (1/90x10^6)*(TIM_PERIOD+1)*32

	 Para que f=750Hz  -> TIM_Period = 3749
	 Para que f=2.5kHz -> TIM_Period = 1124


	 -----------------------------------------------------------------------
	 */
	SystemCoreClockUpdate();
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	/* Se calcula TIM_PERIOD para una frequencia de 10kHz */
	TIM_TimeBaseStructure.TIM_Period = (uint16_t) 1 / ((1 / 90e6) * 10000 * 32) - 1;
	TIM_TimeBaseStructure.TIM_Prescaler = 0;
	TIM_TimeBaseStructure.TIM_ClockDivision = 0;
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;

	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);

	/* TIM Interrupts enable */
	TIM_ITConfig(TIM3, TIM_IT_CC1, ENABLE);

	/* TIM3 enable counter */
	TIM_Cmd(TIM3, ENABLE);
}
