/*
 * switch.c
 *
 *  Created on: May 3, 2023
 *      Author: ariedinger
 */

#include "libs/switch/switch.h"

uint8_t sLed = 0;

void EXTI_LINE13_INIT(void) {
	GPIO_InitTypeDef GPIO_InitStructure;
	EXTI_InitTypeDef EXTI_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Activamos el reloj para GPIOC */
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	/* Activamos el reloj para SYSCFG */
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	/* Configuramos el pin PC13 como salida */
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13;
	GPIO_Init(GPIOC, &GPIO_InitStructure);

	/* Conectamos el pin PC13 a la interrupcion EXTI Line13 */
	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

	/* Configuramos la interrrupcion EXTI Line13 */
	EXTI_InitStructure.EXTI_Line = EXTI_Line13;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure);

	/* Activamos y seteamos la interrupcion EXTI Line13 para la prioridad mas alta */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}


void INIT_LEDS(void) {
	GPIO_InitTypeDef GPIO_InitStructure; //Estructura de configuracion

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	//Ahora se configura el pin PB8

	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_14;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT; //Salida
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;

	GPIO_Init(GPIOB, &GPIO_InitStructure); //Se aplica la configuraci�n definida anteriormente
}

void SWITCH_LED_STATE(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin)
{
	if(sLed == 0)
	{
		sLed = 1;
		GPIO_SetBits(GPIOx, GPIO_Pin);
	}
	else
	{
		sLed = 0;
		GPIO_ResetBits(GPIOx, GPIO_Pin);
	}
}

