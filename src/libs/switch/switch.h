#ifndef LIBS_SWITCH_SWITCH_H_
#define LIBS_SWITCH_SWITCH_H_

/* * * * Libraries * * * */
#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"

/* * * * Functions * * * */
void EXTI_LINE13_INIT(void);
void INIT_LEDS(void);
void SWITCH_LED_STATE(GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);

/* * * * Variables * * * */

#endif /* LIBS_SWITCH_SWITCH_H_ */
