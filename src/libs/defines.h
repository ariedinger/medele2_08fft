/*
 * defines.h
 *
 *  Created on: May 3, 2023
 *      Author: ariedinger
 */

#ifndef LIBS_DEFINES_H_
#define LIBS_DEFINES_H_

/* * * * Defines * * * */
#define MAX_SAMPLES 1000

/* * * * Flags * * * */
uint8_t fTimer = 0;
uint8_t fAdc = 0;
long i = 0;

/* * * * Variables * * * */
float aAdc[MAX_SAMPLES];
float* fft_magnitude;

/* * * * Local Functions * * * */
void ADC_PROCESSING(void);

#endif /* LIBS_DEFINES_H_ */
