#ifndef LIBS_ADC_H_
#define LIBS_ADC_H_

/* * * * Libraries * * * */
#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"

/* * * * Functions * * * */
void ADC_INIT(void);
int32_t ADC_READ(void);

#endif /* LIBS_ADC_H_ */
